//
//  ProfileTopView.swift
//  Test Task
//
//  Created by Eugene on 27/09/2017.
//  Copyright © 2017 Eugene Martinson. All rights reserved.
//

import Foundation
import UIKit

protocol XibDesignable : class {}

extension XibDesignable where Self : UIView {
    
    static func instantiateFromXib() -> Self {
        
        let dynamicMetatype = Self.self
        let bundle = Bundle(for: dynamicMetatype)
        let nib = UINib(nibName: "\(dynamicMetatype)", bundle: bundle)
        
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            
            fatalError("Could not load view from nib file.")
        }
        return view
    }
}

class RoundedCornerImageView : UIImageView {
    
    override func awakeFromNib() {
        layer.masksToBounds = true
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 3.0
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.width / 2.0
    }
}

class ProfileTopView: UIView, XibDesignable {
    
    @IBOutlet weak var avatarWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImageView: RoundedCornerImageView!
    @IBOutlet weak var nameTextLabel: UILabel!
    
    var originalAvatarSize: CGFloat = 70.0
    
    private var _sizeRatio: CGFloat = 1.0 {
        didSet {
            avatarWidthConstraint.constant = originalAvatarSize * _sizeRatio
            print("set \(_sizeRatio) as avaSizeRatio")
        }
    }
    var avatarSizeRatio: CGFloat {
        set {
            guard newValue >= 0.0 && newValue <= 1.0 else {
                print("sizeRatio bounds are [0..1]")
                return
            }
            _sizeRatio = newValue
        }
        get {
            return _sizeRatio
        }
    }
    
    override func awakeFromNib() {
        originalAvatarSize = avatarWidthConstraint.constant
    }
    
    
}
