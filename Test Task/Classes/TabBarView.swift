//
//  TabBarView.swift
//  Test Task
//
//  Created by Eugene on 28/09/2017.
//  Copyright © 2017 Eugene Martinson. All rights reserved.
//

import Foundation
import UIKit

protocol TabBarViewDataSource: class {
    func numberOfTabsInTabBar(_ tabBar: TabBarView) -> UInt
    func tableViewForTab(index: UInt, in tabBar: TabBarView) -> UITableView
    func nameForTab(index: UInt, in tabBar: TabBarView) -> String
}

class MenuView: UIView {
    var labels: [UIButton] = []
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    func commonInit() {
       backgroundColor = .gray
    }
}

class TabBarView: UIView {

    weak var dataSource: TabBarViewDataSource? = nil
    weak var delegate: UIScrollViewDelegate? {
        didSet {
            scrollView.delegate = delegate
        }
    }
    
    var menuHeight: CGFloat = 44.0
    
    let menuView = MenuView()
    let scrollView = UIScrollView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    private func commonInit() {
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        //scrollView.delaysContentTouches = false
        addSubview(menuView)
        addSubview(scrollView)
        
    }
    
    private var tableViews: [UITableView] = []
    
    override func layoutSubviews() {
        let _viewWidth = self.bounds.width
        let _viewHeight = self.bounds.height
        menuView.frame = CGRect(x: 0, y: 0, width: _viewWidth, height: menuHeight)
        
        let numberOfTabs = tableViews.count
        
        scrollView.frame = CGRect(x: _viewWidth, y: menuHeight, width: _viewWidth, height: _viewHeight - menuHeight)
        scrollView.contentSize = CGSize(width: _viewWidth * CGFloat(numberOfTabs), height: _viewHeight)
        
        let labelWidth = _viewWidth / CGFloat(numberOfTabs)
        
        for i in 0..<numberOfTabs {
            let tableView = tableViews[i]
            
            tableView.frame = CGRect(x: _viewWidth * CGFloat(i), y: menuHeight, width: _viewWidth, height: _viewHeight - menuHeight)
            
            menuView.labels[i].frame = CGRect(x: labelWidth * CGFloat(i), y: 0, width: labelWidth, height: menuHeight)
        }
    }
    
    func reloadData() {
        guard let dataSource = dataSource else {
            print("dataSource is not implemented")
            return
        }
        let numberOfTabs = dataSource.numberOfTabsInTabBar(self)
        
        let _viewWidth = self.frame.width
        let _viewHeight = self.frame.height
        scrollView.contentSize = CGSize(width: _viewWidth * CGFloat(numberOfTabs), height: _viewHeight)
        
        tableViews.removeAll()
        menuView.labels.removeAll()
        for subview in menuView.subviews { subview.removeFromSuperview() }
        
        for i in 0..<numberOfTabs {
            let tableView = dataSource.tableViewForTab(index: i, in: self)

            tableViews.append(tableView)
            self.addSubview(tableView)
            let button = UIButton(type: .system)
            let title = dataSource.nameForTab(index: i, in: self)
            button.setTitle(title, for: .normal)

            menuView.labels.append(button)
            menuView.addSubview(button)
        }
        layoutSubviews()
    }
    
}
