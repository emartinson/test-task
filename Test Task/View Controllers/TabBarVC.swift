//
//  TabBarVC.swift
//  Test Task
//
//  Created by Eugene on 29/09/2017.
//  Copyright © 2017 Eugene Martinson. All rights reserved.
//

import Foundation
import UIKit

class TabBarVC: UIViewController {
 
    var tabBar: TabBarView!
    
    override func viewDidLoad() {
        tabBar = TabBarView()
        
        view.addSubview(tabBar)
        
        //tabBar.delegate = self
        tabBar.dataSource = self
        tabBar.reloadData()
    }
}

extension TabBarVC: TabBarViewDataSource {
    func numberOfTabsInTabBar(_ tabBar: TabBarView) -> UInt {
        return 4
    }
    
    func tableViewForTab(index: UInt, in tabBar: TabBarView) -> UITableView {
        let tableView = UITableView(frame: CGRect.zero, style: .plain)
        //tableView.delegate = self
        return tableView
    }
    
    func nameForTab(index: UInt, in tabBar: TabBarView) -> String {
        return "Tab \(index)"
    }
}
