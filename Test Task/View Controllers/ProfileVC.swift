//
//  ProfileVC.swift
//  Test Task
//
//  Created by Eugene on 27/09/2017.
//  Copyright © 2017 Eugene Martinson. All rights reserved.
//

import Foundation
import UIKit

class ProfileVC: UIViewController {
    
    var _headerSwitchOffset: CGFloat = 0.0
    let _profileViewHeight: CGFloat = 150.0
    var _headerImageHeight: CGFloat = 150.0
    let _subHeaderHeight: CGFloat = 100.0
    let _avatarImageSize: CGFloat = 70
    let _avatarImageCompressedSize: CGFloat = 44
    let _barIsCollapsed = false
    let _barAnimationComplete = false
    
    let _topViewInitialConstraint: CGFloat = 86.0
    
    
    var topImage: UIImageView!
    //@IBOutlet weak var topView: ProfileTopView!
    var topView: ProfileTopView = ProfileTopView.instantiateFromXib()
    //@IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        navigationController?.navigationBar.isTranslucent = false
        
        topImage = UIImageView(image: UIImage(named: "header"))
        mainScrollView.addSubview(topImage)
        
        mainScrollView.addSubview(topView)
    }
    
 
    var tabBarTopOffet: CGFloat!
    
    override func viewDidLayoutSubviews() {
        mainScrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
        topImage.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: _headerImageHeight)
        topView.frame = CGRect(x: 0, y: _headerImageHeight, width: view.bounds.width, height: _profileViewHeight)
        //tabBarTopOffet = _headerImageHeight + _profileViewHeight
        //tabBar.frame = CGRect(x: 0, y: tabBarTopOffet, width: view.bounds.width, height: view.bounds.height - tabBarTopOffet)
    }
}

extension ProfileVC: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            
            let resizeRatio = 1 - min (scrollView.contentOffset.y / 100.0, 0.5)
            print("TabBar Offset: \(scrollView.contentOffset), ratio: \(resizeRatio)")
            topView.avatarSizeRatio = resizeRatio
            viewDidLayoutSubviews()
        } else {
            print("Content Offset: \(scrollView.contentOffset)")
            
            
            //tabBarTopOffet = _topViewInitialConstraint - min(_topViewInitialConstraint, scrollView.contentOffset.y / 100)
            
        }
    }
}




